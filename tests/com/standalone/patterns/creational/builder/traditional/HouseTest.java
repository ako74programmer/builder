package com.standalone.patterns.creational.builder.traditional;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class HouseTest {
    private static final String BLACK = "black";
    private static final int FLOOR_12 = 12;
    private static final int DOOR_12 = 22;
    private static final int WINDOW_33 = 33;
    private static final String YELLOW = "yellow";
    private static final int FLOOR_100 = 100;
    private static final int DOOR_200 = 200;
    private static final int WINDOW_3 = 300;

    class StandardHouseBuilderStub extends HouseBuilder {

        @Override
        public void buildRoof() {
            house.setRoofColor(BLACK);
        }

        @Override
        public void buildFloor() {
            house.setFloorsNumber(FLOOR_12);
        }

        @Override
        public void buildDoor() {
            house.setDoorsNumber(DOOR_12);
        }

        @Override
        public void buildWindow() {
            house.setWindowsNumber(WINDOW_33);
        }
    }
    class LargeHouseBuilderStub extends HouseBuilder {

        @Override
        public void buildRoof() {
            house.setRoofColor(YELLOW);
        }

        @Override
        public void buildFloor() {
            house.setFloorsNumber(FLOOR_100);
        }

        @Override
        public void buildDoor() {
            house.setDoorsNumber(DOOR_200);
        }

        @Override
        public void buildWindow() {
            house.setWindowsNumber(WINDOW_3);
        }
    }

    private House getHouse(HouseBuilder builder) {
        HouseDirector director = new HouseDirector(builder);
        director.buildHouse();
        House house = director.getHouse();
        return house;
    }

    @Test
    void itShouldBuildTraditionalStandardHouse() {
        House house = getHouse(new StandardHouseBuilderStub());

        assertAll(
                () -> assertEquals(BLACK, house.getRoofColor()),
                () -> assertEquals(FLOOR_12, house.getFloorsNumber()),
                () -> assertEquals(DOOR_12, house.getDoorsNumber()),
                () -> assertEquals(WINDOW_33, house.getWindowsNumber())
        );
    }

    @Test
        void itShouldBuildTraditionalLargeHouse() {
        House house = getHouse(new LargeHouseBuilderStub());

        assertAll(
                    () -> assertEquals(YELLOW, house.getRoofColor()),
                    () -> assertEquals(FLOOR_100, house.getFloorsNumber()),
                    () -> assertEquals(DOOR_200, house.getDoorsNumber()),
                    () -> assertEquals(WINDOW_3, house.getWindowsNumber())
            );

    }
}