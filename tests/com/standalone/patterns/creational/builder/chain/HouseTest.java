package com.standalone.patterns.creational.builder.chain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HouseTest {

    @Test
    void itShouldBuildChainHouse() {
        String address = "Address";
        int floorsNumber = 1;
        int windowsNumber = 4;
        int doorsNumber = 2;

        House house = House.builder()
                .setAddress(address)
                .setFloorsNumber(floorsNumber)
                .setDoorsNumber(doorsNumber)
                .setWindowsNumber(windowsNumber)
                .build();

        assertAll(
                () -> assertNotNull(house),
                () -> assertEquals(address, house.getAddress()),
                () -> assertEquals(floorsNumber, house.getFloorsNumber()),
                () -> assertEquals(doorsNumber, house.getDoorsNumber()),
                () -> assertEquals(windowsNumber, house.getWindowsNumber())
        );

    }
}