package com.standalone.patterns.creational.builder.traditional;

public abstract class HouseBuilder {
    protected House house;

    public House getHouse() {
        return house;
    }

    public void createNewHouse() {
        house = new House();
    }

    public abstract void buildRoof();
    public abstract void buildFloor();
    public abstract void buildDoor();
    public abstract void buildWindow();
}
