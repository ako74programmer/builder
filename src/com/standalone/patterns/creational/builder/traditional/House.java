package com.standalone.patterns.creational.builder.traditional;

public class House {
    private String roofColor = null;
    private Integer floorsNumber = null;
    private Integer doorsNumber = null;
    private Integer windowsNumber = null;

    public void setRoofColor(String roofColor) {
        this.roofColor = roofColor;
    }

    public void setFloorsNumber(Integer floorsNumber) {
        this.floorsNumber = floorsNumber;
    }

    public void setDoorsNumber(Integer doorsNumber) {
        this.doorsNumber = doorsNumber;
    }

    public void setWindowsNumber(Integer windowsNumber) {
        this.windowsNumber = windowsNumber;
    }

    public String getRoofColor() {
        return roofColor;
    }

    public Integer getFloorsNumber() {
        return floorsNumber;
    }

    public Integer getDoorsNumber() {
        return doorsNumber;
    }

    public Integer getWindowsNumber() {
        return windowsNumber;
    }

    @Override
    public String toString() {
        return "House{" +
                "roofColor='" + roofColor + '\'' +
                ", floorsNumber=" + floorsNumber +
                ", doorsNumber=" + doorsNumber +
                ", windowsNumber=" + windowsNumber +
                '}';
    }
}
