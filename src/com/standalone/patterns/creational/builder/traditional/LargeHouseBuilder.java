package com.standalone.patterns.creational.builder.traditional;

public class LargeHouseBuilder extends HouseBuilder {
    static final String ROOF_COLOR = "red";
    static final int FLOORS_NUMBER = 2;
    static final int DOORS_NUMBER = 2;
    static final int WINDOWS_NUMBER = 8;

    @Override
    public void buildRoof() {
        house.setRoofColor(ROOF_COLOR);
    }

    @Override
    public void buildFloor() {
        house.setFloorsNumber(FLOORS_NUMBER);
    }

    @Override
    public void buildDoor() {
        house.setDoorsNumber(DOORS_NUMBER);
    }

    @Override
    public void buildWindow() {
        house.setWindowsNumber(WINDOWS_NUMBER);
    }
}
