package com.standalone.patterns.creational.builder.traditional;

public class HouseDirector {

    private HouseBuilder houseBuilder;

    public HouseDirector(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }


    public House getHouse() {
        return houseBuilder.getHouse();
    }

    public void buildHouse() {
        houseBuilder.createNewHouse();
        houseBuilder.buildDoor();
        houseBuilder.buildWindow();
        houseBuilder.buildFloor();
        houseBuilder.buildRoof();
    }

    public void setHomeBuilder(HouseBuilder houseBuilder) {
        this.houseBuilder = houseBuilder;
    }
}
