package com.standalone.patterns.creational.builder.traditional;

public class StandardHouseBuilder extends HouseBuilder {

    static final String ROOF_COLOR = "red";
    static final int FLOORS_NUMBER = 1;
    static final int DOORS_NUMBER = 1;
    static final int WINDOWS_NUMBER = 4;

    @Override
    public void buildRoof() {
        house.setRoofColor(ROOF_COLOR);
    }

    @Override
    public void buildFloor() {
        house.setFloorsNumber(FLOORS_NUMBER);
    }

    @Override
    public void buildDoor() {
        house.setDoorsNumber(DOORS_NUMBER);
    }

    @Override
    public void buildWindow() {
        house.setWindowsNumber(WINDOWS_NUMBER);
    }

}
